package com.example.dummy_api_withdraw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DummyApiWithdrawApplication {

    public static void main(String[] args) {
        SpringApplication.run(DummyApiWithdrawApplication.class, args);
    }

}
