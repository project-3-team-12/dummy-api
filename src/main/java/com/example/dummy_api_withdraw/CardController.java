package com.example.dummy_api_withdraw;

import org.springframework.web.bind.annotation.*;

import java.util.Random;

@RestController
@RequestMapping("/card")
public class CardController {

    @PostMapping("/withdraw")
    public Response withdraw(@RequestBody CardAddRequest cardAddRequest) {

        Random random = new Random();
        int rand_int1 = random.nextInt(2);

        Response response = new Response();
        if (rand_int1 == 1) {
            response.setResponse("Successful");
            response.setAmount(cardAddRequest.getAmount());
        } else {
            response.setResponse("Unsuccessful");
            response.setAmount(0);
        }
        return response;

    }
}

